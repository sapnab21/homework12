import java.util.*;
public class SetDemo{
    public static void main(String[] args) {
        Set<String> cohort1 = new HashSet<>();
        cohort1.add("United States");
        cohort1.add("United States");
        cohort1.add("Ukraine");
        cohort1.add("Mexico");
        System.out.println("Countries in cohort 1: " + cohort1);
        Set<String> cohort2 = new HashSet<>();
        cohort2.add("United States");
        cohort2.add("Canada");
        cohort2.add("United States");
        cohort2.add("Mexico");
        System.out.println("Countries in cohort 2: " + cohort2);
        Set<String> set3 = new HashSet<>(cohort1);
        set3.addAll(cohort2);
        System.out.println("All unique countries of both cohorts: " + set3);
        Set<String> alphaCohort = new TreeSet<>(set3);
        System.out.println("All unique countries of both cohorts alphabetically: " + alphaCohort);
        Set<String> common= new HashSet<>(cohort1);
        common.retainAll(cohort2);
        System.out.println("All common countries of both cohorts: " + common);

    }
}
